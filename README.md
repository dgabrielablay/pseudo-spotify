# Spotify Clone

This project is developed to practice for ReactJS functionalities

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software:

* [Node](https://nodejs.org/en/)

### Installing

Here's a step by step series of examples that tell you how to get a development env running:

In your terminal, clone the repository

```
git clone https://dgabrielablay@bitbucket.org/dgabrielablay/pseudo-spotify.git
```

Go into that directory

```
cd pseudo-spotify
```

Install dependencies

```
npm install
```

Run the project

```
npm start
```

## Deployment

Should you need to deploy this project in a live system through Heroku or any other hosting site, just simply create a pipeline through this bitbucket link and it should automatically run the code.

## Built With

* [ReactJS](https://reactjs.org/) - Main framework used
* [Material UI Icons](https://material-ui.com/components/material-icons/) - Icon library used

## Versioning

* [Git](https://git-scm.com/) for versioning.

## Authors

* **Don Gabriel Ablay** - *Sole Developer* - [dgda](https://github.com/dgda)
